#pragma once

#include "mraa.hpp"
#include <unistd.h>
#include <stdint.h>

using namespace mraa;

class Joystick
{
public:
	static Joystick* instance();

	

	uint16_t* read();

protected:
	Joystick();
	~Joystick();

private:
	static Joystick* myInstance;

	uint16_t NOP = 0x0000;

	Spi* spi;
};

