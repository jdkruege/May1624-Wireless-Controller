#pragma once

#include <iostream>
#include <stdio.h>
#include <list>

using namespace std;

class Message
{
public:
	Message(string sender, string destination, list<double> data);
	~Message();

	string getSender();
	string getDestination();
	list<double> getData();

	ostream& operator>>(ostream& os);

	istream& operator<<(istream& is);
private:
	string _sender;
	string _destination;

	list<double> _data;

};

