#pragma once
#include "Message.h"

#include <map>
#include <queue>

class Mailbox
{
public:
	static Mailbox* instance();

	Message* checkMail(string ID);
	bool deliverMail(Message msg);

	bool sendMessage(Message toSend);
	Message* shipMessage();

protected:
	Mailbox();
	~Mailbox();

private:
	static Mailbox* myInstance;

	map<string, Message>* inbox;
	queue<Message>* outbox;
};

