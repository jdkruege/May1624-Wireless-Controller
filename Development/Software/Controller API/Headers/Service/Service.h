#pragma once

#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <list>
#include <thread>

using namespace std;

struct ServiceInfo
{
	int id;
	string name;
	double frequency;
};

class Service
{
public:

	Service(int id, string name, double freq);

	virtual void thread() = 0;

	ServiceInfo info();

private:
	int _id;
	string _name;
	double _frequency;
};

