#pragma once
#include "Service.h"
#include "LSM9DS0.h"
#include "Mailbox.h"

class GyroService :
	public Service
{
public:
	GyroService(int id, double freq);
	~GyroService();

	void start();
	void run();
	void stop();

private:
	LSM9DS0* _sensor;
	Mailbox* _mailbox;
	thread* _thread;
};

