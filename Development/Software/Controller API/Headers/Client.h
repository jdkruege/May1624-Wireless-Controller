#ifndef Cl
#define Cl

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

using namespace std;

namespace Client
{
	extern struct sockaddr_rc addr;
	extern int s, status;

	bool cInit(string destination);

	void cConnect();

	bool cCheckStatus();

	void cSendMessage(string message);
	
	void cDisconnect();
};

#endif