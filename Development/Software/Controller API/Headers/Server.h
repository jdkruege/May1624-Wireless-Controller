#ifndef Se
#define Se

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

using namespace std;

namespace Server
{
	extern struct sockaddr_rc loc_addr;
	extern char buf[1024];
	extern int s, client, bytes_read;
	extern socklen_t opt;

	void sInit();

	string sListen();

	string sReadMessage();

	void sDisconnect();
}

#endif