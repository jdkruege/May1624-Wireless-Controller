#include "../../Headers/MessagePassing/Mailbox.h"

#pragma region Creation/Deletion
Mailbox* Mailbox::instance()
{
	if (myInstance == 0)
	{
		myInstance = new Mailbox();
	}

	return myInstance;
}

Mailbox::Mailbox()
{
	inbox = new map<string, Message>();
	outbox = new queue<Message>();
}


Mailbox::~Mailbox()
{
}
#pragma endregion

#pragma region Send/Receive
Message* Mailbox::checkMail(string id)
{
	// Check if there is mail in the mailbox for the given id
	if (inbox->count(id) > 0)
	{
		Message* msg;
		msg = &(inbox->at(id));

		inbox->erase(id);

		return msg;
	}

	return nullptr;
}

bool Mailbox::deliverMail(Message msg)
{
	inbox->insert(msg.getDestination, msg);
}

bool Mailbox::sendMessage(Message toSend)
{

}
#pragma endregion