#include "Message.h"


Message::Message(string sender, string destination, list<double> data)
{
	_sender = sender;
	_destination = destination;
	_data = data;
}

string Message::getSender()
{
	return _sender;
}

string Message::getDestination()
{
	return _destination;
}

list<double> Message::getData()
{
	return _data;
}
