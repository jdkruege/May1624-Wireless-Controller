#include "Client.h"

struct sockaddr_rc Client::addr;
int Client::s, Client::status;

bool Client::cInit(string destination)
{
	if (destination.length() == 17)
	{
		// set the connection parameters (who to connect to)
		addr.rc_family = AF_BLUETOOTH;
		addr.rc_channel = (uint8_t)1;
		str2ba(destination.c_str(), &addr.rc_bdaddr);

		return true;
	}

	return false;
}

void Client::cConnect()
{
	// allocate a socket
	s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

	// connect to server
	status = connect(Client::s, (sockaddr *)&addr, sizeof(addr));
}

bool Client::cCheckStatus()
{
	if (status < 0) return false;
	return true;
}

void Client::cSendMessage(string message)
{
	status = write(Client::s, message.c_str(), message.length());
}

void Client::cDisconnect()
{
	close(Client::s);
}