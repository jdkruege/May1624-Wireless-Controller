#include "Joystick.h"

Joystick* Joystick::myInstance = 0;

#pragma region Creation/Deletion
Joystick* Joystick::instance()
{
	if (myInstance == 0)
	{
		myInstance = new Joystick();
	}
	return myInstance;
}

Joystick::Joystick()
{
	// set up the mraa library
	mraa_init();

	// set the SPI channels
	spi = new Spi(1);

	// set the frequency ( 10Mhz )
	spi->frequency(10000000);

	// set the mode
	spi->mode(MRAA_SPI_MODE2);

	// ensure MSB is sent first
	spi->lsbmode(false);

	// set up registers
	// set up for conversions!
	uint16_t send = NOP | 0x0320; // 2030
	spi->write_word(send);

	usleep(100000);

	send = NOP | 0x0312; // 1203
	spi->write_word(send);
}

Joystick::~Joystick() {
	delete spi;
}
#pragma endregion

uint16_t* Joystick::read()
{
	uint16_t receive[2];

	receive[0] = spi->write_word(NOP);
	receive[2] = spi->write_word(NOP);

	return receive;
}
