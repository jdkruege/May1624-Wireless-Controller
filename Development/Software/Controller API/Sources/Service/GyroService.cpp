#include "GyroService.h"

void GyroService::run()
{
	while (1)
	{
		// Sample the registers to figure out X, Y, and Z
		short X = _sensor->read_G(0x29) && _sensor->read_G(0x28);
		short Y = _sensor->read_G(0x2B) && _sensor->read_G(0x2A);
		short Z = _sensor->read_G(0x2D) && _sensor->read_G(0x2C);

		// Create a message to send with our name, the name of who it needs to go to, and the data to be carried
		Message* msg = new Message("Gyro Service", "User", { (double)X, (double)Y, (double)Z });

		// Add our msg into the mail to be sent
		_mailbox->send(msg);

		// Sleep 5 seconds
		usleep(500000);
	}
}

void GyroService::stop()
{

}

GyroService::GyroService(int id, double freq) : Service(id, "Gyro", freq)
{
	// Get the instances for the sensor and the mailbox we need to use
	_sensor = LSM9DS0::instance();
	_mailbox = Mailbox::instance();

	_thread = new thread(&run);
}

GyroService::~GyroService()
{
}
