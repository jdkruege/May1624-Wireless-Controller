#include "Service.h"

Service::Service(int id, string name, double frequency) : _id(id), _name(name), _frequency(frequency) {}

ServiceInfo Service::info()
{
	ServiceInfo info;

	info.id = _id;
	info.name = _name;
	info.frequency = _frequency;

	return info;
}
