#include "../Headers/Message.h"

Message::Message(string sender, string destination, char dataType, int size, void* data)
{
	_sender = sender;
	_destination = destination;
	_dataType = dataType;
	_size = size;
	_data = data;
}

string Message::getSender()
{
	return _sender;
}

string Message::getDestination()
{
	return _destination;
}

char Message::getType()
{
	return _dataType;
}

int Message::getSize()
{
	return _size;
}

void* Message::getData()
{
	return _data;
}
