#include "ServiceIDs.h"

typedef struct {
	int numServices;
	int serviceIDs[];
} ServiceSet;

const ServiceSet GyroSet = { 3, { GYRO_X, GYRO_Y, GYRO_Z } };
const ServiceSet AccelSet = { 3, { ACCEL_X, ACCEL_Y, ACCEL_Z } };
const ServiceSet MagSet = { 3, { MAG_X, MAG_Y, MAG_Z } };

