#ifdef __cplusplus

#define ExternC extern "C"
#define DLLExport __declspec(dllexport)
#include "Message.h"
#include <string.h>

#else

#define ExternC 
#define DLLExport 
#include <stdbool.h>

#endif

#include "ServiceSet.h"

typedef struct {
	char* addr[];
} Bt_Addr;

ExternC DLLExport bool connect();
ExternC DLLExport bool disconnect();

ExternC DLLExport double readSensor(int serviceID);
ExternC DLLExport int readSensors(double buff[], int length, ServiceSet serviceIDs);
ExternC DLLExport bool receiveAudio(char buff[]);
ExternC DLLExport bool sendAudio(char buff[]);

ExternC DLLExport Bt_Addr parseAddress(char* address);