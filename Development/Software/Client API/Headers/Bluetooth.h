#include "../Headers/Message.h"

namespace Bluetooth
{
	bool sendDataStub(Message msg);
	Message receiveDataStub();
}