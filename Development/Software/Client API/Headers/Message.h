#pragma once

#include <iostream>
#include <stdio.h>
#include <list>

using namespace std;

class Message
{
public:
	Message(string sender, string destination, char dataType, int size, void* data);
	~Message();

	string getSender();
	string getDestination();
	char getType();
	int getSize();
	void* getData();

	ostream& operator>>(ostream& os);

	istream& operator<<(istream& is);
private:
	string _sender;
	string _destination;
	char _dataType;
	int _size;
	void* _data;

};

