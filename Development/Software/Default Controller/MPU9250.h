/*
	@author Jonathan Krueger
*/

#pragma once

#include "mraa.hpp"
#include <unistd.h>
#include <stdint.h>

using namespace mraa;

class MPU9250
{
public:
	static MPU9250* instance();

	uint8_t read(uint8_t address);
	void write(uint8_t address, uint8_t data);

protected:
	MPU9250();
	~MPU9250();

private:
	static MPU9250* myInstance;

	Spi* spi;
};

