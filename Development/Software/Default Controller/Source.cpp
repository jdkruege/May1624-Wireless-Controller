#include <stdio.h>
#include <iostream>
#include <string>
#include <stdint.h>
#include <unistd.h>
#include "MPU9250.h"
#include "buttonInterrupt.h"

using namespace std;

int main(int argc, char **argv)
{
	if (buttonInterrupt() == -1)
	{
		return -1;
	}

	MPU9250 *instance = MPU9250::instance();

	while (1)
	{
		float gyXh = instance->read(0x43);
		float gyXl = instance->read(0x44);
		float gyYh = instance->read(0x45);
		float gyYl = instance->read(0x46);
		float gyZh = instance->read(0x47);
		float gyZl = instance->read(0x48);

		string state = (buttonState == 0) ? ("Not Pressed") : ("Pressed");

		cout << "Gyro X: " << hex << gyXh << " Gyro Y: "<< hex << gyYh << " Gyro Z: " << hex << gyZh << "\t\t\tButton State: " << state << endl;

		usleep(1000000);
	}

	return 0;
}