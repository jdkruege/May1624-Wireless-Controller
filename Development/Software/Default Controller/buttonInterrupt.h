/*
* buttonInterrupt.c
*
*  Created on: Feb 10, 2016
*      Author: Nik
*/
#include <stdio.h>
#include <stdlib.h>
#include "mraa.h"

static volatile int buttonState = 0;

int buttonInterrupt();