#include "buttonInterrupt.h"

void pressed(void* args) {
	buttonState = 1;
}

void released(void* args) {
	buttonState = 0;
}

int buttonInterrupt() {

	//initialize mraa
    mraa_init();

    //create mraa context
    mraa_gpio_context context;

    //set context to GPIO 15 (J20:7)
    context = mraa_gpio_init(15);

    if (context == NULL) {
        return -1;
    }

    mraa_gpio_dir(context, MRAA_GPIO_IN);

	mraa_gpio_isr(context, MRAA_GPIO_EDGE_FALLING, &pressed, NULL);
	mraa_gpio_isr(context, MRAA_GPIO_EDGE_RISING, &released, NULL);

    return 0;
}
