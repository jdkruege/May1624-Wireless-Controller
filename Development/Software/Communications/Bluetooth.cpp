//*
// * Bluetooth.cpp
// *
// *  Created on: Nov 15, 2015
// *  	Author: Branden Sammons
// */
//
//class Bluetooth {
//
//public:
//
//	/**
//	 * 	connect()   :
//	 * 				 	Takes 1 argument, A Bluetooth address, and tries to connect to that address
//	 * 					only if this device has successfully paired with the address given before.
//	 * 					If the other address is recognized then we attempt to connect and returns
//	 * 					true if successful.
//	 *
//	 * 	Arguments 	:
//	 * 					Bluetooth address of another device
//	 *
//	 * 	Returns		:
//	 * 					Boolean determining whether connection was made.
//	 */
//	virtual bool connect(/* Bluetooth address */) {}
//
//
//
//	/**
//	 * 	pair()		:
//	 * 				 	Attempts to pair with the Bluetooth Address given, returns true if successful.
//	 *
//	 * 	Arguments 	:
//	 * 					Bluetooth address of another device
//	 *
//	 * 	Returns		:
//	 * 					Boolean determining whether pairing was successful.
//	 */
//	virtual bool pair(/* Bluetooth address */) {}
//
//
//
//	/**
//	 * 	send()		:
//	 * 				 	Sends the given data to the last device a connection was established with.
//	 *
//	 * 	Arguments 	:
//	 * 					String to be sent
//	 *
//	 * 	Returns		:
//	 * 					Boolean determining whether sending was successful.
//	 */
//	virtual bool send(std::string *string) {}
//
//
//
//	/**
//	 * 	receive()	:
//	 * 				 	Receives the data coming to the device by storing it in the pointer specified.
//	 *
//	 * 	Arguments 	:
//	 * 					Pointer to store data that is incoming.
//	 *
//	 * 	Returns		:
//	 * 					Boolean determining whether receive was successful.
//	 */
//	virtual bool receive(std::string *string) {}
//};
//
//
//
//
